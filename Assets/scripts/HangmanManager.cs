﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HangmanManager : MonoBehaviour
{

    /// <summary>
    /// Textfield to display all game output text
    /// </summary>
    [SerializeField] Text m_mainText;
    /// <summary>
    /// Input field, where the player has to enter a word to guess
    /// </summary>
    [SerializeField] InputField m_wordInputField;

    /// <summary>
    /// Input field, where the player has to enter a character to guess
    /// </summary>
    [SerializeField] InputField m_charInputField;

    /// <summary>
	/// The complete word that the player has to guess.
	/// </summary>
	string m_wordToGuess;

    /// <summary>
    /// The word that is displayed with the already guessed characters
    /// </summary>
    string m_wordToDisplay;

    /// <summary>
    /// The character that was guessed
    /// </summary>
    string m_characterToGuess;

    /// <summary>
    /// single player button
    /// </summary>
    [SerializeField] Button m_singleplayerButton;

  
    /// <summary>
    /// hotseat mode button
    /// </summary>
    [SerializeField] Button m_hotseatButton;

    /// <summary>
    /// restart button
    /// </summary>
    [SerializeField] Button m_restartButton;

    /// <summary>
    /// hangman image 
    /// </summary>
    [SerializeField] Image m_hangmanImage;

    /// <summary>
    /// sprite sheet for the hangman
    /// </summary>
    [SerializeField] Sprite[] m_imageArray;
    /// <summary>
    /// The maximal wrong guesses the player can take before losing the game
    /// </summary>
    [SerializeField] int m_maximalWrongGuesses =5;

    string[] m_hundredWords = new string[] { "Albinism", "Alcoholic hepatitis", "Allergy", "Alopecia", "Alzheimer's disease", "Amblyopia", "Amebiasis", "Anemia", "Aneurdu", "Anorexia", "Anosmia", "Anotia", "Anthrax", "Appendicitis", "Apraxia", "Argyria", "Arthritis", "Aseptic meningitis", "Asthenia", "Asthma", "Astigmatism", "Atherosclerosis", "Athetosis", "Atrophy", "Bacterial meningitis", "Barack Obama syndrome", "Beriberi", "Biebertitis", "Black Death", "Botulism", "Breast cancer", "Bronchitis", "Brucellosis", "Bubonic plague", "Bunion", "Bella killer", "Calculi", "Campylobacter infection","Cancer", "Candidiasis", "Carbon monoxide poisoning", "Celiacs disease", "Cerebral palsy", "hagas disease", "Chalazion", "Chavia", "Cherubism", "Chickenpox, Chlamydia", "Chlamydia trachomatis", "Cholera", "Chordoma, Chorea", "Chronic fatigue syndrome", "Circadian rhythm sleep disorder", "Coccidioidomycosis", "Colitis", "Common cold", "Condyloma", "Congestive heart disease", "Coronary heart disease", "Cowpox", "Cretinism", "Crohn's Disease", "Dengue", "Diabetes mellitus", "Diphtheria", "Diarrhoea", "Dehydration", "Ear infection", "Ebola" };
private int m_numberOfWrongGuesses;

    /// <summary>
    /// The amount of wrong guesses.
    /// </summary>


    // Use this for initialization
    void Start()
    {
        ///debugger
        /// check if the UI Game Objects are assigned in the Inspector Editor Window, if not do a error log
        if (m_mainText == null) { Debug.LogError("No m_mainText Object assigned in Inspector"); }
        if (m_wordInputField == null) { Debug.LogError("No m_inputField Object assigned in Inspector"); }
        if (m_hotseatButton == null) { Debug.LogError("No m_hotseatButton Object assigned in Inspector"); }
        if (m_charInputField == null) { Debug.LogError("No m_charInputField Object assigned in Inspector"); }
        if (m_hangmanImage == null) { Debug.LogError("No m_hangmanImage Object assigned in Inspector"); }
        if (m_singleplayerButton == null) { Debug.LogError("No m_singleplayerButton Object assigned in Inspector"); }
        if (m_restartButton == null) { Debug.LogError("No m_restartButton Object assigned in Inspector"); }
       
        ///start setup game function
        SetupGame();
    }
    /// <summary>
    /// game setup command and hides everything except the singleplayer mode and the hotseat game mode
    /// </summary>
     public void SetupGame()
    {
        ///disabling unnecessary UI elements      

        m_restartButton.gameObject.SetActive(false);
        m_charInputField.gameObject.SetActive(false);
        m_wordInputField.gameObject.SetActive(false);
        m_hangmanImage.gameObject.SetActive(false);

        ///resetting the number of wrong guesses to start the game
        m_numberOfWrongGuesses = 0;

        ///enables the hotseat button and the singleplayer button
        m_singleplayerButton.gameObject.SetActive(true);
        m_hotseatButton.gameObject.SetActive(true);

        ///changes the main text to..
        m_mainText.text = "choose your game mode";
       
    }
    /// <summary>
    /// start the hotseat game mode when the function is called from the hotseat game buttton
    /// </summary>
    public void StartHotseatGame()
    {
        ///enabling the necessary UI elements for the hotseat mode
        m_wordInputField.gameObject.SetActive(true);
        m_hangmanImage.gameObject.SetActive(true);

        ///disable unnecessary UI elements
        m_hotseatButton.gameObject.SetActive(false);
        m_singleplayerButton.gameObject.SetActive(false);

        ///changes the main text box to..
        m_mainText.text = "Please enter a word to guess";

        ///sets the first image of the hangwoman sprite sheet image array 
        m_hangmanImage.sprite = m_imageArray[0];
    }
    public void StartSinglePlayer()
    {
        ///disable unnecessary UI elements 
		m_hotseatButton.gameObject.SetActive(false);
        m_singleplayerButton.gameObject.SetActive(false);

        ///activate necessary UI elements for the singleplayer mode
        m_charInputField.gameObject.SetActive(true);
        m_hangmanImage.gameObject.SetActive(true);

        ///sets the first image of the hangwoman sprite sheet image array 
        m_hangmanImage.sprite = m_imageArray[0];

        ///radnomiser to choose a random number relative to the amount of words in the m_hundredrWords string
        int randomNumber = Random.Range(0, m_hundredWords.Length);

        ///command to assign the container of the random number in the m_wordToGuess variable 
        m_wordToGuess = m_hundredWords[randomNumber].ToUpper();

        //clear the word that we want to display
        m_wordToDisplay = "";

        //call the funnction that generates the hidden word
        GenerateHiddenText();

        //call the UpdateMainText funktion with the string THE AI PICKED A HIDDEN WORD. TRY TO GUESS IT as a parameter
        UpdateMainText("The A.I. picked a hidden word, try to guess it!");
    }


    public void WordEntered()
    {
        ///convert all characters in the text of the word input field to UpperCase and save it in the variable m_wordToGuess
		m_wordToGuess = m_wordInputField.text.ToUpper();

        ///clear the input field for the word
        m_wordInputField.text = "";

        //call the funnction that generates the hidden word
        GenerateHiddenText();

        //deactivate the input field for the word
        m_wordInputField.gameObject.SetActive(false);

        //activate the input field for the character
        m_charInputField.gameObject.SetActive(true);

        //call the UpdateMainText funktion with the string LETS START THE GAME as a parameter
        UpdateMainText("Lets start the game!");
    }

    /// <summary>
	/// Generates a word that has the same length as the word to guess, but only contains underline characters
	/// </summary>
    void GenerateHiddenText()
    {
        //executes foreach loop for every character in the word 
        foreach (char character in m_wordToGuess)
        {
            if (character == ' ')
            ///adds a string with a "space" to the word if the current character in the foreach loop is a space 
            {
                m_wordToDisplay += " ";
            }
            ///adds a string with "_" character if the current character in the foreach loop is not a space
            else
            { 
                m_wordToDisplay += "_";
            }
        }
    }

    /// <summary>
	/// Shows the current hidden word and the number of times the player can try to guess
	/// </summary>
	/// <param name="_startText">Start text</param>
	void UpdateMainText(string _startText)
    { 
        m_charInputField.ActivateInputField();
         
        ///assign the start text plus two newlines to the main text object. backslash N is interpreted as a newline  
        m_mainText.text = _startText + "\n\n";
        ///add YOU WANT TO GUESS A WORD and the word to display in BOLD to the main text object. <b>everything in here is bold</b>is used as RichText
        m_mainText.text += "You have to guess the word : <b>" + m_wordToDisplay + "</b>";
        ///add two newlines and the text ENTER A CHARACTER THAT YOU WANT TO GUESS to the main text
        m_mainText.text += "\n\n" + "Enter a character that you want to guess ... ";
        ///add two newlines and the text YOU CAN TRY X TIMES to the main text. X is the number of guesses left for the player
        m_mainText.text += "\n\n" + "You can try " + (m_maximalWrongGuesses - m_numberOfWrongGuesses) + " times!";
    }
    /// <summary>
	/// Show the game over text, the restart button and the hidden word 
	/// </summary>
	void GameOverText()
    {
        ///assign to the main text YOU ENTERED afterwards the entered character in BOLD and the text AND GUESSED WRONG FOR THE LAST TIME.
        m_mainText.text = "You entered <b>" + m_characterToGuess + "</b> and guessed wrong for the last time!";
        ///add two newlines and the text YOU ARE DEAD! THE HIDDEN WORD WAS and the complete word to guees to the main text
        m_mainText.text += "\n\n" + "You are dead! The hidden word was : " + m_wordToGuess;
        ///add two newlines and the text GAME OVER! to the main text
        m_mainText.text += "\n\n" + "GAME OVER!";

        ///deactivates the character input field
        m_charInputField.gameObject.SetActive(false);

        ///activates the restart button
        m_restartButton.gameObject.SetActive(true);
    }

    void GameWonText()
    {
        ///assign to the main text YOU ENTERED afterwards the entered character in BOLD and the text AND GUESSED RIGHT.
        m_mainText.text = "You entered <b>" + m_characterToGuess + "</b> and guessed right!";
        ///add two newlines and the text THE HIDDEN WORD WAS and the complete word to guees to the main text
        m_mainText.text += "\n\n" + "The hidden word was : " + m_wordToGuess;
        ///add two newlines and the text CONGRATULATIONS! to the main text
        m_mainText.text += "\n\n" + "CONGRATULATIONS!";

        ///deactivates the character input field
        m_charInputField.gameObject.SetActive(false);

        ///activates the restart button
        m_restartButton.gameObject.SetActive(true);
    }

    /// <summary>
	/// This is called by the CharInputField - Checks if a character is in the word to guess and replaces the characters in the display word
	/// </summary>
	public void CharacterEntered()
    {
        ///transform the entered character to Uppercase and assign it to the variable m_characterToGuess
        m_characterToGuess = m_charInputField.text.ToUpper();

        //clear the text of the character input field
        m_charInputField.text = "";

        ///set the index variable to 0 - we use this to count the current character position in the word
        int index = 0;
        ///we set a characterFound variable to false - only if we really find a character we want this to become true
        bool characterFound = false;

        ///for every character in the word we want to guess we execute a loop
        foreach (char character in m_wordToGuess)
        {
            ///the entire code between the curly braces after FOREACH is called as many times as characters are in m_wordToGuess 

            ///check if the current character of the loop is equal the entered character. [0] is always the first character in a string
            if (character == m_characterToGuess[0])
            {
                ///remove the UNDERLINE character that is on the current positon in the word to display string
                m_wordToDisplay = m_wordToDisplay.Remove(index, 1);
                ///insert the correct character at the correct position
                m_wordToDisplay = m_wordToDisplay.Insert(index, character.ToString());

                ///set the characterFound variable to true
                characterFound = true;
            }

            ///remember to count the actual position in the loop and increase index by 1 ( we could also write index = index + 1; )  
            index++;
        }

        ///check if we have found a character
		if (characterFound)
        ///checks if m_wordToDisplay = m_wordToGuess 
        ///if it is true, it calls the GameWonText function and the game is won
        if (m_wordToDisplay == m_wordToGuess)
        {
            ///call the Game Won Text function
            GameWonText();
        }
        else
        {
            ///call the UpdateMainText function with the parameter YOU ENTERED X AND GUESSED RIGHT - X will be the entered character in BOLD
            UpdateMainText("You entered <b>" + m_characterToGuess + "</b> and guessed right!");
        }
        ///entering a wrong character thus increasing the value of the hangman sprite sheet array
        else
        {
            ///we did not found a character and increase the number of wrong guesses by 1 
            ///we could also write m_numberOfWrongGuesses = m_numberOfWrongGuesses + 1;
            m_numberOfWrongGuesses++;

            ///we choose a new sprite for the hangman image 
			/// we pick from the array the index t9hat is equal to the amount of wrong guesses
			m_hangmanImage.sprite = m_imageArray[m_numberOfWrongGuesses];

            //check if the number of wrong guesses is equal the maximum of allowed wrong guesses
            if (m_numberOfWrongGuesses == m_maximalWrongGuesses)
            {
                ///if we have to many wrong guesses - call the game over text funktion
                GameOverText();
            }
            else
            {
                ///call the UpdateMainText function with the parameter YOU ENTERED X AND GUESSED WRONG - X will be the entered character in BOLD
                UpdateMainText("You entered character : <b>" + m_characterToGuess + "</b> and guessed wrong!");
            }
        } 

    }
}
